﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.IO;


namespace opentk
{
    /// <summary>
    /// Holds a terrain for the game to draw. Can be made from file, randomly or by passing in a 2d array of terrainsegments
    /// </summary>
    class Terrain
    {//holds a terrain to render at the bottom of the world and collide with
        public TerrainSegment[,] terrainData; //its heights
        public float scaleX;
        public float scaleY;
        public float scaleZ;//scale in all axes

        public float waterHeight = 0.1f; //how high to render the water

        public Vector3 offset = new Vector3(0, -2, 0);//an offset from rendering at the origin


        public Terrain(int width, int depth)
        {
            //random terrain
            terrainData = new TerrainSegment[width, depth];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < depth; j++)
                {
                    terrainData[i, j] = new TerrainSegment();
                }
            }
        }

        public Terrain(string TerrainName)
        {//loads th terraindata from file
            try
            {            
                List<string[]> lines = new List<string[]>();

                using (StreamReader sr = new StreamReader("terrainData/" + TerrainName + ".wld"))
                {//gets the contents of the file, splitting off the scale at the top of the file
                    string line;
                    //read the header of the file, to get scale
                    string header = sr.ReadLine();
                    string[] headerArr = header.Split(',');
                    scaleX = (float)Convert.ToDouble(headerArr[0]);
                    scaleY = (float)Convert.ToDouble(headerArr[1]);
                    scaleZ = (float)Convert.ToDouble(headerArr[2]);

                    while ((line = sr.ReadLine()) != null)
                   {
                      lines.Add(line.Split(' '));
                  }
             }

             terrainData = new TerrainSegment[lines.Count, lines[0].Length];
                //reading the data into an array of segments
                for (int i = 0; i < lines.Count; i++)
                {
                    for (int j = 0; j < lines[0].Length; j++)
                   {
                       terrainData[i, j] = new TerrainSegment((float)Convert.ToDouble(lines[i][j]));
                  }
                }
            }
            catch (Exception e)
            {//YFU messgae, just in case
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                //throw;

            }
        }

        public Terrain(TerrainSegment[,] _terrainData)
        {
            //in case I need to load terraindata without a file
            terrainData = _terrainData;
        }

        public void Draw()
        {//drawing the terrain
            GL.PushMatrix();
            for (int i = 0; i < terrainData.GetLength(0)-1; i++)
            {//for every row
                for (int j = 0; j < terrainData.GetLength(1)-1; j++)
                {//and every column
                    if ((terrainData[i, j].height) * scaleY > waterHeight * scaleY)
                    {//if you need to render it
                        using (Tri tri = new Tri(new Vector3(i * scaleX, (terrainData[i, j].height) * scaleY, j * scaleZ) + offset, new Vector3((i + 1) * scaleX, (terrainData[i + 1, j].height) * scaleY, j * scaleZ) + offset, new Vector3((i + 1) * scaleX, (terrainData[i + 1, j + 1].height) * scaleY, (j + 1) * scaleZ) + offset))
                        {//create the two triangles, settheir colour and draw them (cn be improved, must check in a later iteration)
                            tri.setColour(terrainData[i, j].col);
                            tri.Draw();
                        }
                        using (Tri tri = new Tri(new Vector3(i * scaleX, (terrainData[i, j].height) * scaleY, j * scaleZ) + offset, new Vector3((i + 1) * scaleX, (terrainData[i + 1, j + 1].height) * scaleY, (j + 1) * scaleZ) + offset, new Vector3(i * scaleX, (terrainData[i, j + 1].height) * scaleY, (j + 1) * scaleZ) + offset))
                        {
                            tri.setColour(terrainData[i, j].col);
                            tri.Draw();
                        }
                    }
                }
            }
            //draw water
            GL.Begin(PrimitiveType.Quads);
            GL.Color3(Color.Blue);
            GL.Vertex3(new Vector3(0, waterHeight * scaleY, 0) + offset);
            GL.Vertex3(new Vector3(terrainData.GetLength(0) * scaleX, waterHeight * scaleY, 0) + offset);
            GL.Vertex3(new Vector3(terrainData.GetLength(0) * scaleX, waterHeight * scaleY, terrainData.GetLength(1) * scaleZ) + offset);
            GL.Vertex3(new Vector3(0, waterHeight * scaleY, terrainData.GetLength(1) * scaleZ) + offset);
            GL.End();
            GL.PopMatrix();
        }
    }
}
