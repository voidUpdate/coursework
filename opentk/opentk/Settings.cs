﻿using OpenTK;

namespace opentk
{
    class Settings
    {//hold a single settings profile
        public bool Fullscreen = false;
        public int XResolution = 1280;
        public int YResolution = 720;

        /// <summary>
        /// Creates a new settings preset, currently only sets resolution and fullscreen-ness
        /// </summary>
        public Settings()
        {
            if (Fullscreen)
            {//if we  are fullscreen, then override the resolution with the monitor resolution
                XResolution = DisplayDevice.Default.Width;
                YResolution = DisplayDevice.Default.Height;
            }
        }
    }
}
