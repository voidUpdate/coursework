﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace opentk
{
    class Game: GameWindow
    {
        #region Game Variables
        //lists of all gameobjects to render etc.
        public static List<GameObject> gameObjects = new List<GameObject>();
        public static List<Collectable> Collectables = new List<Collectable>();
        public static List<Text> Texts = new List<Text>();
        public static List<Glider> gliders = new List<Glider>();

        //cameras rotation in the y axis
        public static double ThetaY = Math.PI/2f;
        //current camera fov
        public static float fov = (float)Math.PI / 4f;
        //the camera viewport
        public static Matrix4 modelview = Matrix4.Identity;
        

        //the terrain to load
        public static Terrain terrain = new Terrain(Program.map);

        //keeping track of movement
        public static int horizontal = 0;
        public static int vertical = 0;

        //Scoring variables etc.
        public static float distance = 0;
        public static int score = 0;
        public static float maxSpeed = 0;

        #endregion

        #region Death Variables

        public static List<Text> DeathMessages = new List<Text>();

        #endregion

        #region Generic Variables
        //random object, always nice to have
        public static Random rnd = new Random();

        //target framerate
        public static double framerate = 60;

        //the current state of the game
        public static int gamestate = 0;

        //all settings
        public static List<Settings> settings = new List<Settings>(){new Settings()};
        public static Size gamesize;

        #endregion



        #region OpenTK Boilerplate

        protected override void OnLoad(EventArgs e)
        {//runs when the game is first loaded
            base.OnLoad(e);
            Title = "Volatus"; //change title
            GL.ClearColor(Color.CornflowerBlue); //change the background colour that everything is drawn on top of
            GL.Enable(EnableCap.Texture2D); 
            GL.Enable(EnableCap.DepthTest);//enabling various opentk shenanigans
            GL.DepthMask(true); 
            GL.DepthFunc(DepthFunction.Lequal);//ensuring that polygons draw in the correct order
            WindowState = settings[0].Fullscreen ? WindowState.Fullscreen : WindowState.Normal; //setting the fullscreen-ness of it
            gamesize = new Size(settings[0].XResolution, settings[0].YResolution);
            Size = gamesize; //setting the size

            GameInit(); //initializing the game            
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {//called every time the game wants to render a frame
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);//clear the screen

            if (gamestate == 1)
            {
                //playing game
                gliders[0].CameraTrans();//set the camera position
                GL.MatrixMode(MatrixMode.Modelview);//change to rendering 3d models
                GL.LoadMatrix(ref modelview);//load the matrix describing the camera
                GameRender();//render the scene
                GameLogic();//perform all logic
                
            }
            else if(gamestate == 0)
            {
                //menu stuff
                GL.MatrixMode(MatrixMode.Modelview);//change to rendering 3d models
                GL.LoadMatrix(ref modelview);//load the matrix describing the camera
                MenuLogic();
                MenuRender();
            }
            else if (gamestate == 2)
            {
                //dead
                GL.MatrixMode(MatrixMode.Modelview);//change to rendering 3d models
                GL.LoadMatrix(ref modelview);//load the matrix describing the camera
                DeathLogic();
                DeathRender();
            }

            SwapBuffers();//send the data to the graphics card

        }

        protected override void OnResize(EventArgs e)
        {//called whenever the game is resized
            base.OnResize(e);
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);//set the viewport of the camera
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(fov, Width / (float)Height, 1.0f, 64.0f);//set how the camera renders to the screen
            GL.MatrixMode(MatrixMode.Projection);//change to editing the projection matrix
            GL.LoadMatrix(ref projection);//store the matrix
        }

        #endregion

        #region Menu Functions

        public void MenuInit()
        {
            gamestate = 0;
        }

        public void MenuLogic()
        {

        }

        public void MenuRender()
        {

        }

        public void MenuUnload()
        {
            GameInit();
        }

        #endregion

        #region Game Functions

        private void GameMouseMove(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {//called whenever the mouse moves
            ThetaY += e.XDelta / 100f;//calculate how much the mouse needs to move
        }

        private void GameKeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {//called whenever a key is pressed
            if (!e.IsRepeat)
            {//this function also fires repeatedly when a key is held down, similar to typing. If the n key is help down, it would type n...nnnnnnnn. this bool check stops that
                switch (e.Key)
                {//switch on the key pressed, and rotate the glider accordingly and set the movement
                    case OpenTK.Input.Key.A:
                        horizontal = 1;
                        gliders[0].GO.Rotate(new Vector3(0, 0, -45));
                        break;
                    case OpenTK.Input.Key.D:
                        horizontal = -1;
                        gliders[0].GO.Rotate(new Vector3(0, 0, 45));
                        break;
                    case OpenTK.Input.Key.S:
                        vertical = -1;
                        gliders[0].GO.Rotate(new Vector3(-45, 0, 0));
                        break;
                    case OpenTK.Input.Key.W:
                        vertical = 1;
                        gliders[0].GO.Rotate(new Vector3(45, 0, 0));
                        break;
                    case OpenTK.Input.Key.Escape:
                        Exit(); //if the esc key is pressed, quit the game
                        break;
                    default:
                        break;
                }
            }


        }

        private void GameKeyUp(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {//called whenever a key is released
            switch (e.Key)
            {//rotate the glider back to the neutral position, and reset the rotation of the glider
                case OpenTK.Input.Key.A:
                    horizontal = 0;
                    gliders[0].GO.Rotate(new Vector3(0, 0, 45));
                    break;
                case OpenTK.Input.Key.D:
                    horizontal = 0;
                    gliders[0].GO.Rotate(new Vector3(0, 0, -45));
                    break;
                case OpenTK.Input.Key.S:
                    vertical = 0;
                    gliders[0].GO.Rotate(new Vector3(45, 0, 0));
                    break;
                case OpenTK.Input.Key.W:
                    vertical = 0;
                    gliders[0].GO.Rotate(new Vector3(-45, 0, 0));
                    break;
                default:
                    break;
            }
        }


        public void GameInit()
        {//called to initialise the game
            ThetaY = Math.PI / 2;
            gliders.Add(new Glider(new Vector3(0, 9, 4), new Vector3(0.5f, 0.2f, 0.8f))); //add a new glider at (0,9,4) with scale (0.5, 0.2, 0.8)

            Texts.Add(new Text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1));
            Texts[0].setColour(Color.BlueViolet);

            //assign event handlers
            KeyDown += GameKeyDown;
            MouseMove += GameMouseMove;
            KeyUp += GameKeyUp;

            //switch to running the game
            gamestate = 1;
        }

        public void GameRender()
        {//called to render everything
            foreach (GameObject obj in gameObjects)
            {//draw all the random stuff
                obj.Draw();
            }
            foreach (Glider obj in gliders)
            {//draw all the gliders
                obj.Draw();
            }

            foreach (Collectable collectable in Collectables)
            {
                collectable.Draw();
            }

            foreach (Text text in Texts)
            {
                text.Draw();
            }
            //draw the terrain
            terrain.Draw();
        }

        public void GameLogic()
        {//called just before rendering the game
            framerate = RenderFrequency;//set a reference to the framerate

            foreach (Collectable collectable in Collectables)
            {
                collectable.Update();
            }

            //Texts[0].Translate(new Vector3(0, 0.1f, 0));


            if (gliders.ToArray()[0].CanDescend())
            {//if the glider can go down, then add some downward velocity
                (gliders.ToArray())[0].velocity += new Vector3(0, (float)(-9.81 * 1 / Game.framerate), 0);
            }
            else
            {//otherwise, it has hit the ground, so go to death screen
                Texts[0].Translate(gliders[0].GO.pos + new Vector3(0, 0, 2));
                GameUnload();
                GameInit();
                return;
                //(gliders.ToArray())[0].Return();
            }
            (gliders.ToArray()[0]).velocity.X = (float)(horizontal * Math.Pow((gliders.ToArray()[0]).velocity.Y, 2) * 0.15);
            (gliders.ToArray()[0]).velocity.Z = (float)(vertical * Math.Pow((gliders.ToArray()[0]).velocity.Y, 2) * 0.15);
            gliders.ToArray()[0].Move();//set the velocities, and move it

        }

        public void GameUnload()
        {//if the game is unloaded, say to go back to a menu, then clear all the gliders
            horizontal = vertical = 0;
            KeyDown -= GameKeyDown;
            MouseMove -= GameMouseMove;
            KeyUp -= GameKeyUp;
            gliders.Clear();
            Texts.Clear();
            
        }


        #endregion

        #region Death Functions

        public void DeathKeyUp(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            switch (e.Key)
            {
                case OpenTK.Input.Key.Enter:
                    DeathUnload();
                    break;
                case OpenTK.Input.Key.Escape:
                    Environment.Exit(0); //if the esc key is pressed, quit the game
                    break;
            }
        }

        public void DeathInit()
        {
            KeyUp += DeathKeyUp;
            gamestate = 2;
            Console.WriteLine("You died!");
            Console.WriteLine("Score: " + score);
            Console.WriteLine("Distance: " + distance);
            Console.WriteLine("Max Speed: " + maxSpeed);
            Console.WriteLine("Press enter to restart");
            DeathMessages.Add(new Text("You Died", 1));
            gliders.Add(new Glider(new Vector3(0,0,0), new Vector3(0.5f, 0.2f, 0.8f), _Cinematic: true));
            //DeathMessages[0].Translate(new Vector3(0, 0, 5));
            ThetaY = Math.PI/2f;
        }

        public void DeathLogic()
        {
            //save score, get nickname etc.
            //ThetaY += 0.1;
        }

        public void DeathRender()
        {
            gliders[0].CameraTrans();

            foreach (Text deathMessage in DeathMessages)
            {
                deathMessage.Draw();
            }
        }

        public void DeathUnload()
        {
            KeyUp -= DeathKeyUp;
            gliders.Clear();
            DeathMessages.Clear();
            GameInit();
        }

        #endregion
        
    }
}
