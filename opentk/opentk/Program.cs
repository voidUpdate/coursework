﻿using System;
using System.ComponentModel.Design;

namespace opentk
{
    class Program
    {
        public static string map = "testing";

        public static string[] menuheader = {" ","██╗   ██╗ ██████╗ ██╗      █████╗ ████████╗██╗   ██╗███████╗","██║   ██║██╔═══██╗██║     ██╔══██╗╚══██╔══╝██║   ██║██╔════╝","██║   ██║██║   ██║██║     ███████║   ██║   ██║   ██║███████╗","╚██╗ ██╔╝██║   ██║██║     ██╔══██║   ██║   ██║   ██║╚════██║"," ╚████╔╝ ╚██████╔╝███████╗██║  ██║   ██║   ╚██████╔╝███████║","  ╚═══╝   ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝"};

        static void Main(string[] args)
        {
            bool inMenu = true;
            string[] menu = { "╔════════════════════╗", "║        MENU:       ║", "║(if unsure, press 3)║", "║    Play Game [1]   ║", "║    Options [2]     ║", "║      Help [3]      ║", "║      Exit [4]      ║", "╚════════════════════╝" };
            while (inMenu)
            {
                Console.Clear();
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.BackgroundColor = ConsoleColor.Black;
                foreach (string s in menuheader)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                foreach (string s in menu)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                Console.ResetColor();
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        using (Game game = new Game())
                        {//runs the game at 60fps (ish)
                            game.Run(60.0);
                        }
                        break;
                    case ConsoleKey.D2:
                        Options();
                        break;
                    case ConsoleKey.D3:
                        Help();
                        break;
                    case ConsoleKey.D4:
                        inMenu = false;
                        break;
                    case ConsoleKey.Escape:
                        inMenu = false;
                        break;
                }

            }
            
        }

        public static void Options()
        {
            bool inMenu = true;
            string[] menu = { "╔════════════════════╗", "║      Options:      ║", "║      Map [1]       ║", "║     Back [2]       ║", "╚════════════════════╝" };
            while (inMenu)
            {
                Console.Clear();
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.BackgroundColor = ConsoleColor.Black;
                foreach (string s in menuheader)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                foreach (string s in menu)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                Console.ResetColor();
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        Map();
                        break;
                    case ConsoleKey.D2:
                        inMenu = false;
                        break;
                    case ConsoleKey.Escape:
                        inMenu = false;
                        break;
                }

            }
        }

        public static void Help()
        {
            Console.Clear();
            string[] menu = { "╔═════════════════════════════════════════════════════════════════════╗", "║                                 Help                                ║", "║                               Menus:                                ║", "║  To select a menu option, press the key inside the square brackets, ║", "║                     or press escape to go back.                     ║", "║                                Game:                                ║", "║To move around, use the WSAD keys, and move the mouse to look around.║", "║                    Press escape to quit the game.                   ║", "║                      (Press any key to go back)                     ║", "╚═════════════════════════════════════════════════════════════════════╝" };
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.Black;
            foreach (string s in menuheader)
            {
                Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
            }
            foreach (string s in menu)
            {
                Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
            }
            Console.ResetColor();
            Console.ReadKey(true);
        }

        public static void Map()
        {
            bool inMenu = true;
            while (inMenu)
            {
                string[] menu = { "╔═════════════════════════╗", "║Choose the map to play on║", $"║   Current map:{map,-7}   ║", "║         200 [1]         ║", "║        height [2]       ║", "║        other [3]        ║", "║        smile [4]        ║", "║       testing [5]       ║", "║         Back [6]        ║", "╚═════════════════════════╝" };
                Console.Clear();
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.BackgroundColor = ConsoleColor.Black;
                foreach (string s in menuheader)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                foreach (string s in menu)
                {
                    Console.WriteLine(String.Format("{0," + ((Console.WindowWidth / 2) + (s.Length / 2)) + "}", s));
                }
                Console.ResetColor();
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        map = "200";
                        break;
                    case ConsoleKey.D2:
                        map = "height";
                        break;
                    case ConsoleKey.D3:
                        map = "other";
                        break;
                    case ConsoleKey.D4:
                        map = "smile";
                        break;
                    case ConsoleKey.D5:
                        map = "testing";
                        break;
                    case ConsoleKey.D6:
                        inMenu = false;
                        break;
                    case ConsoleKey.Escape:
                        inMenu = false;
                        break;
                }

            }

        }
    }
}
