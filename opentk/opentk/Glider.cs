﻿using System;
using OpenTK;

namespace opentk
{
    class Glider
    {
        public Vector3 velocity = Vector3.Zero;//stores the current velocity

        public GameObject GO;//the visual representation of the glider

        public float acceleration = 1;//is responsiveness to a change in velocity

        public Vector3 startPoint = new Vector3(Vector3.Zero);//where to start from and return to

        public float gravity = -9.81f;

        public bool Cinematic = false;

        public Glider(Vector3 _startPoint, Vector3 scale, bool _Cinematic = false)
        {//the constructor, sets the start point, creates the gameobject and scales it accordingly
            startPoint = _startPoint;
            GO = new GameObject(GameObject.Primitive.Cube, startPoint);
            GO.Scale(scale);
            Cinematic = _Cinematic;
        }

        public void CameraTrans()
        {//sets the camera position relative to the glider
            Game.modelview = Matrix4.LookAt(GO.pos + new Vector3(0, 0.5f, -1.5f), GO.pos + new Vector3((float)Math.Cos(Game.ThetaY), 0.5f, (float)Math.Sin(Game.ThetaY)), Vector3.UnitY);
        }                                                                     

        public bool CanDescend()
        {//check for downwards collision
            if (Cinematic) return false;

            try
            {
                if (GO.pos.Y > Game.terrain.terrainData[(int)(GetTerrainPos().X), (int)(GetTerrainPos().Z)].height * Game.terrain.scaleY + Game.terrain.offset.Y)//)))]}})
                {//if the glider can move downward, return true
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            
            return false;
        }

        public void Move()
        {//applies velocity to position
            if (Cinematic) return;

            GO.pos += new Vector3((float)((velocity.X/ Game.framerate) + (0.5 * acceleration * Math.Pow(1/Game.framerate, 2))), (float)((velocity.Y / Game.framerate) + (0.5 * gravity * Math.Pow(1 / Game.framerate, 2))), (float)((velocity.Z/ Game.framerate) + (0.5 * acceleration * Math.Pow(1/Game.framerate, 2))));
            //Console.WriteLine(velocity);
            
        }

        public void Transform(Vector3 vector)
        {//change its position
            if (Cinematic) return;
            GO.Translate(vector);
        }

        public void Return()
        {//sets position to the start position, resets the look direction and zero's the velocity
            velocity = Vector3.Zero;
            GO.pos = startPoint;
            Game.ThetaY = Math.PI / 2f;
        }

        public Vector3 GetTerrainPos()
        {//gets a rounded position of where it is relative to the terrain
            Terrain currterrain = Game.terrain;

            //get world pos
            Vector3 pos = GO.pos;
            //get offset & scale
            Vector3 terrScale = new Vector3(currterrain.scaleX, 1, currterrain.scaleZ);
            Vector3 terrOffset = currterrain.offset;
            //subtract offset from world coord
            pos -= terrOffset;
            //multiply by scale
            pos = new Vector3(pos.X/terrScale.X, pos.Y, pos.Z/terrScale.Z);
            //round to int values
            pos = new Vector3((float)Math.Round(pos.X), 1, (float)Math.Round(pos.Z));

            return pos;
        }

        public void Draw()
        {
            if (Cinematic) return;
            GO.Draw();
        }
    }
}
