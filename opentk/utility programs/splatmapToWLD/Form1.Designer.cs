﻿namespace splatmapToWLD
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_Browse = new System.Windows.Forms.Button();
            this.filePath = new System.Windows.Forms.Label();
            this.btn_Convert = new System.Windows.Forms.Button();
            this.scaleX = new System.Windows.Forms.NumericUpDown();
            this.scaleY = new System.Windows.Forms.NumericUpDown();
            this.scaleZ = new System.Windows.Forms.NumericUpDown();
            this.lbl_scaleX = new System.Windows.Forms.Label();
            this.lbl_scaleY = new System.Windows.Forms.Label();
            this.lbl_scaleZ = new System.Windows.Forms.Label();
            this.image_preview = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.scaleX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_preview)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_Browse
            // 
            this.btn_Browse.Location = new System.Drawing.Point(12, 12);
            this.btn_Browse.Name = "btn_Browse";
            this.btn_Browse.Size = new System.Drawing.Size(75, 23);
            this.btn_Browse.TabIndex = 0;
            this.btn_Browse.Text = "Browse...";
            this.btn_Browse.UseVisualStyleBackColor = true;
            this.btn_Browse.Click += new System.EventHandler(this.btn_Browse_Click);
            // 
            // filePath
            // 
            this.filePath.AutoSize = true;
            this.filePath.Location = new System.Drawing.Point(91, 15);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(41, 17);
            this.filePath.TabIndex = 1;
            this.filePath.Text = "Path:";
            // 
            // btn_Convert
            // 
            this.btn_Convert.Location = new System.Drawing.Point(12, 199);
            this.btn_Convert.Name = "btn_Convert";
            this.btn_Convert.Size = new System.Drawing.Size(297, 31);
            this.btn_Convert.TabIndex = 2;
            this.btn_Convert.Text = "Convert";
            this.btn_Convert.UseVisualStyleBackColor = true;
            this.btn_Convert.Click += new System.EventHandler(this.btn_Convert_Click);
            // 
            // scaleX
            // 
            this.scaleX.DecimalPlaces = 1;
            this.scaleX.Location = new System.Drawing.Point(179, 59);
            this.scaleX.Name = "scaleX";
            this.scaleX.Size = new System.Drawing.Size(120, 22);
            this.scaleX.TabIndex = 3;
            this.scaleX.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // scaleY
            // 
            this.scaleY.DecimalPlaces = 1;
            this.scaleY.Location = new System.Drawing.Point(179, 108);
            this.scaleY.Name = "scaleY";
            this.scaleY.Size = new System.Drawing.Size(120, 22);
            this.scaleY.TabIndex = 4;
            this.scaleY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // scaleZ
            // 
            this.scaleZ.DecimalPlaces = 1;
            this.scaleZ.Location = new System.Drawing.Point(179, 157);
            this.scaleZ.Name = "scaleZ";
            this.scaleZ.Size = new System.Drawing.Size(120, 22);
            this.scaleZ.TabIndex = 5;
            this.scaleZ.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // lbl_scaleX
            // 
            this.lbl_scaleX.AutoSize = true;
            this.lbl_scaleX.Location = new System.Drawing.Point(180, 36);
            this.lbl_scaleX.Name = "lbl_scaleX";
            this.lbl_scaleX.Size = new System.Drawing.Size(66, 17);
            this.lbl_scaleX.TabIndex = 6;
            this.lbl_scaleX.Text = "Scale (X)";
            // 
            // lbl_scaleY
            // 
            this.lbl_scaleY.AutoSize = true;
            this.lbl_scaleY.Location = new System.Drawing.Point(180, 88);
            this.lbl_scaleY.Name = "lbl_scaleY";
            this.lbl_scaleY.Size = new System.Drawing.Size(66, 17);
            this.lbl_scaleY.TabIndex = 7;
            this.lbl_scaleY.Text = "Scale (Y)";
            // 
            // lbl_scaleZ
            // 
            this.lbl_scaleZ.AutoSize = true;
            this.lbl_scaleZ.Location = new System.Drawing.Point(180, 137);
            this.lbl_scaleZ.Name = "lbl_scaleZ";
            this.lbl_scaleZ.Size = new System.Drawing.Size(66, 17);
            this.lbl_scaleZ.TabIndex = 8;
            this.lbl_scaleZ.Text = "Scale (Z)";
            // 
            // image_preview
            // 
            this.image_preview.Location = new System.Drawing.Point(12, 42);
            this.image_preview.Name = "image_preview";
            this.image_preview.Size = new System.Drawing.Size(139, 138);
            this.image_preview.TabIndex = 9;
            this.image_preview.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 245);
            this.Controls.Add(this.image_preview);
            this.Controls.Add(this.lbl_scaleZ);
            this.Controls.Add(this.lbl_scaleY);
            this.Controls.Add(this.lbl_scaleX);
            this.Controls.Add(this.scaleZ);
            this.Controls.Add(this.scaleY);
            this.Controls.Add(this.scaleX);
            this.Controls.Add(this.btn_Convert);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.btn_Browse);
            this.Name = "Form1";
            this.Text = "Heightmap To WLD";
            ((System.ComponentModel.ISupportInitialize)(this.scaleX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.image_preview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_Browse;
        private System.Windows.Forms.Label filePath;
        private System.Windows.Forms.Button btn_Convert;
        private System.Windows.Forms.NumericUpDown scaleX;
        private System.Windows.Forms.NumericUpDown scaleY;
        private System.Windows.Forms.NumericUpDown scaleZ;
        private System.Windows.Forms.Label lbl_scaleX;
        private System.Windows.Forms.Label lbl_scaleY;
        private System.Windows.Forms.Label lbl_scaleZ;
        private System.Windows.Forms.PictureBox image_preview;
    }
}

