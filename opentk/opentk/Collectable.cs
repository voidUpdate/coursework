﻿using OpenTK;

namespace opentk
{
    class Collectable
    {
        public GameObject GO;
        public Vector3 position;

        public enum CollectableType {Coin}

        public Collectable(GameObject _GO, Vector3 _position)
        {
            GO = _GO;
            position = _position;
            GO.Translate(position);
        }

        public Collectable(CollectableType type, Vector3 _position)
        {
            position = _position;
            switch (type)
            {
                case CollectableType.Coin:
                    GO = new GameObject(GameObject.Primitive.Cylinder, position);
                    GO.Rotate(new Vector3(0, 0, 90));
                    break;
                default:
                    break;
            }
        }

        public void Draw()
        {
            GO.Draw();
        }

        public void Update()
        {
              
        }


    }
}
