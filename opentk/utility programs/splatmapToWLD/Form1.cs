﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace splatmapToWLD
{
    public partial class Form1 : Form
    {
        public static Bitmap heightmap;
        string input;
        string outputPath;
        string output;
        string outputfilename;


        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            var codecs = ImageCodecInfo.GetImageEncoders();
            var codecFilter = "Image Files|";
            foreach (var codec in codecs)
            {
                codecFilter += codec.FilenameExtension + ";";
            }

            openFileDialog1.Filter = codecFilter;

            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                filePath.Text = openFileDialog1.FileName;
                input = openFileDialog1.FileName;
                outputPath = Path.GetDirectoryName(input);
                heightmap = (Bitmap)Image.FromFile(input);
                outputfilename = Path.GetFileNameWithoutExtension(input) + ".wld";
                image_preview.ImageLocation = input;                
            }
        }

        private void btn_Convert_Click(object sender, EventArgs e)
        {
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(outputPath, outputfilename)))
            {
                outputFile.Write(scaleX.Value.ToString() + "," + scaleY.Value.ToString() + "," + scaleZ.Value.ToString());

                for (int i = 0; i < heightmap.Height; i++)
                {
                    outputFile.Write("\n");
                    for (int j = 0; j < heightmap.Width; j++)
                    {
                        outputFile.Write(Convert.ToString(heightmap.GetPixel(i, j).GetBrightness()));
                        if (j != heightmap.Width - 1)
                        {
                            outputFile.Write(" ");
                        }
                    }
                }
            }
            filePath.Text = "Done";
        }
    }
}
