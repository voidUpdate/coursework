﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OBJtoVOL
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input the filename of the obj (including extension)");
            string file = Console.ReadLine(); //filename
            TextReader tr = new StreamReader(file); //allows me to read the file
            List<string> Lines = new List<string>(); //a place to store the lines
            while (tr.Peek() != -1) //while we havent reached the end of the file...
            {
                Lines.Add(tr.ReadLine());//add the current line to the end of the list
            }
            Console.WriteLine("Read " + Lines.Count + " lines from " + file); //print some debug

            var vertices = new List<(float x, float y, float z)>(); //create a new list of tuples, to store vertices
            var faces = new List<(int p1, int p2, int p3)>();//create another list of tuples, this time to store all the faces
            //time to parse
            for (int i = 0; i < Lines.Count; i++)//for every line in the file
            {
                if (Lines[i].StartsWith("v "))//if the first character is a v, its defining a vertex
                {
                    string[] temp = Lines[i].Split(' ');//split the line up on spaces
                    vertices.Add((float.Parse(temp[1]), float.Parse(temp[2]), float.Parse(temp[3])));//and add the relevant parts to the list of vertices
                }
                else if (Lines[i].StartsWith("f "))//if it starts with an f, its defining a face
                {
                    string[] temp = Lines[i].Split(' '); //split the line up into its parts
                    if (temp.Length > 4) //if there are more than three vertices, then it cannot be converted, due to how the game engine works
                    {
                        Console.WriteLine("Model is incompatible with this converter. Please only use faces that have less than 4 vertices each");
                        break;//exit
                    }
                    else
                    {
                        faces.Add((int.Parse(temp[1].Split('/')[0]), int.Parse(temp[2].Split('/')[0]), int.Parse(temp[3].Split('/')[0])));//else, make a new face from the correct vertices
                    }

                }
            }
            Console.Write("new List<Tri>(){ ");//start writing the list definition
            foreach ((int p1, int p2, int p3) tuple in faces)//for every face
            {//write out all the vertices, with class definitions etc.
                Console.Write("new Tri(new Vector3(" + vertices[tuple.p1-1].x + "f, " + vertices[tuple.p1-1].y + "f, " + vertices[tuple.p1-1].z + "f), " +
                              "new Vector3(" + vertices[tuple.p2-1].x + "f, " + vertices[tuple.p2-1].y + "f, " + vertices[tuple.p2-1].z + "f), " +
                              "new Vector3(" + vertices[tuple.p3-1].x + "f, " + vertices[tuple.p3-1].y + "f, " + vertices[tuple.p3-1].z + "f)), \n");
            }
            Console.Write("};");//and finish the list definition
            Console.ReadKey(); //wait to exit
        }
    }
}
