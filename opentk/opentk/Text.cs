﻿using System.Collections.Generic;
using System.Drawing;
using OpenTK;

namespace opentk
{
    class Text
    {
        public string text = ""; //the text to display
        public List<RenderableChar> renderableChars = new List<RenderableChar>();//the list of renderable characters this instance is storing
        float fontSize;//the font size

        /// <summary>
        /// A text object holds a group of RenderableChars, to be drawn together
        /// </summary>
        /// <param name="_text">The text to be displayed, as a string</param>
        /// <param name="_fontSize">The font size to use. This is a fraction of the full size, as opposed to a pixel measurement</param>
        public Text(string _text, float _fontSize)
        {
            text = _text; //store the text locally
            fontSize = _fontSize; //store the font size locally

            for (int i = 0; i < text.Length; i++)
            {//for every character, create a new renderablechar to display
                renderableChars.Add(new RenderableChar(text.ToUpper()[i], i, fontSize));
            }
        }

        /// <summary>
        /// Draws the list of RenderableChars
        /// </summary>
        public void Draw()
        {//to draw the text
            foreach (RenderableChar Renderable in renderableChars)
            {
                Renderable.Draw();
            }
        }

        /// <summary>
        /// Translates all the RenderableChars by the translation vector
        /// </summary>
        /// <param name="translation">The Opentk.Vector3 to translate by</param>
        public void Translate(Vector3 translation)
        {//to set the position of the text
            foreach (RenderableChar renderableChar in renderableChars)
            {
                renderableChar.Translate(translation);
            }
        }


        /// <summary>
        /// Sets the colour of all tris in the RenderableChars to the colour passed
        /// </summary>
        /// <param name="colour">The System.DrawingColor to set all the tris to</param>
        public void setColour(Color colour)
        {//for setting the colour of the whole text block
            foreach (RenderableChar renderableChar in renderableChars)
            {
                renderableChar.model.setColour(colour);
            }
        }
    }
}
