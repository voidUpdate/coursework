﻿using System.Drawing;

namespace opentk
{
    class TerrainSegment
    {//hold a single colour and height for a terrain
        public float height;
        public Color col;

        public TerrainSegment()
        {
            height = (float)Game.rnd.NextDouble();
            col = Color.FromArgb(Game.rnd.Next(0, 255), Game.rnd.Next(0, 255), Game.rnd.Next(0, 255));
        }

        public TerrainSegment(float _height)
        {
            height = _height;
            col = Color.FromArgb((int)(height * 0), (int)(height * 255), (int)(height * 0));
        }
    }
}
